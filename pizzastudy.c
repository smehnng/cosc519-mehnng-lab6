#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#define number 14

void *hellothread(void *arg)
{
  int *thread_id = (int *) arg;
  printf("I am student %d!\n", *thread_id);
  void pthread_exit(void *retval);//thread exit
}
int main(int argc, char *argv[])
{
  //Declaring Variable 
  int n = atoi(argv[1]);//number will take from the command line 
  pthread_t threads[n];//pthread array
  int thread_args[n], i;
 
  //creating threads
  if (n>=1 && n<=10)
  {
  for (i=1; i<n; ++i)
    {
      thread_args[i] = i;
      printf("Number of student %d\n", i);
      pthread_create(&threads[i], NULL, hellothread, (void *) &thread_args[i]);
    }

  //wait for threads to finish
  for (i=1; i<n; ++i) 
  {
    pthread_join(threads[i], NULL);//joining thread
  }
   
  }
 
 }
 






















